-- phpMyAdmin SQL Dump
-- version 4.9.7deb1
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le : mer. 13 avr. 2022 à 08:22
-- Version du serveur :  10.3.29-MariaDB-0ubuntu0.20.10.1
-- Version de PHP : 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `insee_v3_ws`
--

-- --------------------------------------------------------

--
-- Structure de la table `StockEtablissement`
--

CREATE TABLE `StockEtablissement` (
  `id` int(11) NOT NULL,
  `siren` int(9) UNSIGNED ZEROFILL NOT NULL,
  `nic` int(5) UNSIGNED ZEROFILL NOT NULL,
  `siret` bigint(14) UNSIGNED ZEROFILL NOT NULL,
  `dateDebut` date NOT NULL,
  `statutDiffusionEtablissement` tinyint(1) NOT NULL,
  `dateCreationEtablissement` date NOT NULL,
  `trancheEffectifsEtablissement` char(2) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Tranche d’effectif salarié de l’établissement',
  `anneeEffectifsEtablissement` smallint(6) NOT NULL COMMENT 'Année de la tranche d’effectif salarié de l’établissement',
  `activitePrincipaleRegistreMetiersEtablissement` char(6) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Code de l’activité exercée par l’artisan inscrit au registre des métiers',
  `dateDernierTraitementEtablissement` datetime NOT NULL COMMENT 'Date de la dernière mise à jour effectuée au répertoire Sirene sur l’établissement',
  `etablissementSiege` tinyint(1) NOT NULL COMMENT 'Indicatrice précisant si le Siret est celui de l’établissement siège ou non',
  `nombrePeriodesEtablissement` smallint(6) NOT NULL COMMENT 'Nombre de périodes dans la vie de l’établissement',
  `complementAdresseEtablissement` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `numeroVoieEtablissement` char(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `indiceRepetitionEtablissement` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `typeVoieEtablissement` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `libelleVoieEtablissement` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `codePostalEtablissement` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `libelleCommuneEtablissement` varchar(70) COLLATE utf8mb4_unicode_ci NOT NULL,
  `libelleCommuneEtrangerEtablissement` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `distributionSpecialeEtablissement` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `codeCommuneEtablissement` char(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `codeCedexEtablissement` char(9) COLLATE utf8mb4_unicode_ci NOT NULL,
  `libellePaysEtrangerEtablissement` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `codepaysetrangeretablissement` char(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `libelleCedexEtablissement` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `complementAdresse2Etablissement` varchar(38) COLLATE utf8mb4_unicode_ci NOT NULL,
  `numeroVoie2Etablissement` smallint(4) NOT NULL,
  `indiceRepetition2Etablissement` char(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `typeVoie2Etablissement` char(4) COLLATE utf8mb4_unicode_ci NOT NULL,
  `libelleVoie2Etablissement` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `codePostal2Etablissement` char(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `libelleCommune2Etablissement` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `libelleCommuneEtranger2Etablissement` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `distributionSpeciale2Etablissement` varchar(26) COLLATE utf8mb4_unicode_ci NOT NULL,
  `codeCommune2Etablissement` char(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `codeCedex2Etablissement` char(9) COLLATE utf8mb4_unicode_ci NOT NULL,
  `libelleCedex2Etablissement` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `codePaysEtranger2Etablissement` char(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `libellePaysEtranger2Etablissement` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `etatAdministratifEtablissement` char(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `enseigne1Etablissement` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `enseigne2Etablissement` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `enseigne3Etablissement` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `denominationUsuelleEtablissement` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `activitePrincipaleEtablissement` varchar(6) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nomenclatureActivitePrincipaleEtablissement` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `caractereEmployeurEtablissement` set('O','N','''''','') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `StockEtablissementHistorique`
--

CREATE TABLE `StockEtablissementHistorique` (
  `id` int(11) NOT NULL,
  `siren` int(9) UNSIGNED ZEROFILL NOT NULL,
  `nic` mediumint(5) UNSIGNED ZEROFILL NOT NULL,
  `siret` bigint(14) UNSIGNED ZEROFILL NOT NULL,
  `dateDebut` date NOT NULL,
  `dateFin` date NOT NULL,
  `etatAdministratifEtablissement` char(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `changementEtatAdministratifEtablissement` tinyint(1) NOT NULL,
  `enseigne1Etablissement` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `enseigne2Etablissement` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `enseigne3Etablissement` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `changementEnseigneEtablissement` tinyint(1) NOT NULL,
  `denominationUsuelleEtablissement` varchar(110) COLLATE utf8mb4_unicode_ci NOT NULL,
  `changementDenominationUsuelleEtablissement` tinyint(1) NOT NULL,
  `activitePrincipaleEtablissement` char(6) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nomenclatureActivitePrincipaleEtablissement` char(8) COLLATE utf8mb4_unicode_ci NOT NULL,
  `changementActivitePrincipaleEtablissement` tinyint(1) NOT NULL,
  `caractereEmployeurEtablissement` set('O','N','''''','') COLLATE utf8mb4_unicode_ci NOT NULL,
  `changementCaractereEmployeurEtablissement` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `StockEtablissement`
--
ALTER TABLE `StockEtablissement`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `siret` (`siret`),
  ADD KEY `siren` (`siren`,`nic`) USING BTREE,
  ADD KEY `code_commune` (`codeCommuneEtablissement`);

--
-- Index pour la table `StockEtablissementHistorique`
--
ALTER TABLE `StockEtablissementHistorique`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `siret_2` (`siret`,`dateDebut`),
  ADD KEY `siren` (`siren`),
  ADD KEY `siret` (`siret`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `StockEtablissement`
--
ALTER TABLE `StockEtablissement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `StockEtablissementHistorique`
--
ALTER TABLE `StockEtablissementHistorique`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
