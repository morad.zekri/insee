-- phpMyAdmin SQL Dump
-- version 4.9.7deb1
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le : mer. 13 avr. 2022 à 08:22
-- Version du serveur :  10.3.29-MariaDB-0ubuntu0.20.10.1
-- Version de PHP : 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `insee_v3_202108`
--

-- --------------------------------------------------------

--
-- Structure de la table `StockEtablissementHistorique_test`
--

CREATE TABLE `StockEtablissementHistorique_test` (
  `id` int(11) NOT NULL DEFAULT 0,
  `siren` int(9) UNSIGNED ZEROFILL NOT NULL,
  `nic` mediumint(5) UNSIGNED ZEROFILL NOT NULL,
  `siret` bigint(14) UNSIGNED ZEROFILL NOT NULL,
  `dateDebut` date NOT NULL,
  `dateFin` date NOT NULL,
  `etatAdministratifEtablissement` char(1) NOT NULL,
  `changementEtatAdministratifEtablissement` tinyint(1) NOT NULL,
  `enseigne1Etablissement` varchar(55) NOT NULL,
  `enseigne2Etablissement` varchar(55) NOT NULL,
  `enseigne3Etablissement` varchar(55) NOT NULL,
  `changementEnseigneEtablissement` tinyint(1) NOT NULL,
  `denominationUsuelleEtablissement` varchar(110) NOT NULL,
  `changementDenominationUsuelleEtablissement` tinyint(1) NOT NULL,
  `activitePrincipaleEtablissement` char(6) NOT NULL,
  `nomenclatureActivitePrincipaleEtablissement` char(8) NOT NULL,
  `changementActivitePrincipaleEtablissement` tinyint(1) NOT NULL,
  `caractereEmployeurEtablissement` set('O','N','''''','') NOT NULL,
  `changementCaractereEmployeurEtablissement` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `StockEtablissementHistorique_test`
--

INSERT INTO `StockEtablissementHistorique_test` (`id`, `siren`, `nic`, `siret`, `dateDebut`, `dateFin`, `etatAdministratifEtablissement`, `changementEtatAdministratifEtablissement`, `enseigne1Etablissement`, `enseigne2Etablissement`, `enseigne3Etablissement`, `changementEnseigneEtablissement`, `denominationUsuelleEtablissement`, `changementDenominationUsuelleEtablissement`, `activitePrincipaleEtablissement`, `nomenclatureActivitePrincipaleEtablissement`, `changementActivitePrincipaleEtablissement`, `caractereEmployeurEtablissement`, `changementCaractereEmployeurEtablissement`, `created_at`, `updated_at`) VALUES
(2863788, 306799107, 00017, 30679910700017, '1900-01-01', '1991-12-24', 'A', 0, '', '', '', 0, '', 0, '', '', 0, '', 0, '2021-08-22 14:03:06', '2021-08-22 12:03:06'),
(2863789, 306799107, 00017, 30679910700017, '2016-01-01', '0000-00-00', 'F', 1, '', '', '', 0, '', 0, '8621Z', 'NAFRev2', 0, 'N', 0, '2021-08-22 14:03:06', '2021-08-22 12:03:06'),
(2863790, 306799107, 00017, 30679910700017, '2008-01-01', '2015-12-31', 'A', 0, '', '', '', 0, '', 0, '8621Z', 'NAFRev2', 1, 'N', 0, '2021-08-22 14:03:06', '2021-08-22 12:03:06'),
(2863791, 306799107, 00017, 30679910700017, '1995-12-25', '2007-12-31', 'A', 0, '', '', '', 0, '', 0, '851C', 'NAF1993', 1, 'N', 0, '2021-08-22 14:03:06', '2021-08-22 12:03:06'),
(2863792, 306799107, 00017, 30679910700017, '1991-12-25', '1995-12-24', 'A', 0, '', '', '', 0, '', 0, '', '', 0, 'N', 1, '2021-08-22 14:03:06', '2021-08-22 12:03:06'),
(3157189, 309007268, 00019, 30900726800019, '1900-01-01', '1993-12-24', 'A', 0, '', '', '', 0, '', 0, '', '', 0, 'N', 0, '2021-08-22 14:03:06', '2021-08-22 12:03:06'),
(3157190, 309007268, 00019, 30900726800019, '2008-01-01', '2017-12-30', 'A', 0, '', '', '', 0, '', 0, '8621Z', 'NAFRev2', 1, 'N', 0, '2021-08-22 14:03:06', '2021-08-22 12:03:06'),
(3157191, 309007268, 00019, 30900726800019, '1993-12-25', '2007-12-31', 'A', 0, '', '', '', 0, '', 0, '851C', 'NAF1993', 1, 'N', 0, '2021-08-22 14:03:06', '2021-08-22 12:03:06'),
(3157192, 309007268, 00019, 30900726800019, '2017-12-31', '0000-00-00', 'F', 1, '', '', '', 0, '', 0, '8621Z', 'NAFRev2', 0, 'N', 0, '2021-08-22 14:03:06', '2021-08-22 12:03:06'),
(3221036, 309335412, 00024, 30933541200024, '1900-01-01', '1991-12-24', 'A', 0, '', '', '', 0, '', 0, '', '', 0, '', 0, '2021-08-22 14:03:06', '2021-08-22 12:03:06'),
(3221037, 309335412, 00024, 30933541200024, '1991-12-25', '1993-12-24', 'A', 0, '', '', '', 0, '', 0, '', '', 0, 'N', 1, '2021-08-22 14:03:06', '2021-08-22 12:03:06'),
(3221038, 309335412, 00024, 30933541200024, '1993-12-25', '1996-02-18', 'A', 0, '', '', '', 0, '', 0, '851C', 'NAF1993', 1, 'N', 0, '2021-08-22 14:03:06', '2021-08-22 12:03:06'),
(3221039, 309335412, 00024, 30933541200024, '1996-02-19', '0000-00-00', 'F', 1, '', '', '', 0, '', 0, '851C', 'NAF1993', 0, 'N', 0, '2021-08-22 14:03:06', '2021-08-22 12:03:06'),
(3221040, 309335412, 00032, 30933541200032, '1996-02-19', '1996-12-24', 'A', 0, '', '', '', 0, '', 0, '', '', 0, '', 0, '2021-08-22 14:03:06', '2021-08-22 12:03:06'),
(3221041, 309335412, 00032, 30933541200032, '1996-12-25', '2007-12-14', 'A', 0, '', '', '', 0, '', 0, '851C', 'NAF1993', 1, 'O', 1, '2021-08-22 14:03:06', '2021-08-22 12:03:06'),
(3221042, 309335412, 00032, 30933541200032, '2007-12-15', '0000-00-00', 'F', 1, '', '', '', 0, '', 0, '851C', 'NAFRev1', 0, 'O', 0, '2021-08-22 14:03:06', '2021-08-22 12:03:06'),
(3691559, 311768717, 00010, 31176871700010, '0000-00-00', '0000-00-00', 'F', 0, '', '', '', 0, '', 0, '0117', 'NAP', 0, 'N', 0, '2021-08-22 14:03:06', '2021-08-22 12:03:06'),
(4040195, 313457244, 00017, 31345724400017, '1989-11-24', '0000-00-00', 'F', 1, '', '', '', 0, '', 0, '8121', 'NAP', 0, 'N', 0, '2021-08-22 14:03:06', '2021-08-22 12:03:06'),
(8951764, 331756031, 00042, 33175603100042, '2007-07-07', '2007-07-30', 'A', 1, '', '', '', 0, '', 0, '526D', 'NAFRev1', 0, 'N', 0, '2021-08-22 14:03:06', '2021-08-22 12:03:06'),
(8951765, 331756031, 00042, 33175603100042, '2013-07-01', '0000-00-00', 'F', 1, '', '', '', 0, '', 0, '4781Z', 'NAFRev2', 0, 'N', 0, '2021-08-22 14:03:06', '2021-08-22 12:03:06'),
(8951766, 331756031, 00042, 33175603100042, '2011-02-10', '2013-06-30', 'A', 1, '', '', '', 0, '', 0, '4781Z', 'NAFRev2', 0, 'N', 0, '2021-08-22 14:03:06', '2021-08-22 12:03:06'),
(8951767, 331756031, 00042, 33175603100042, '2008-09-30', '2011-02-09', 'F', 1, '', '', '', 0, '', 0, '4781Z', 'NAFRev2', 0, 'N', 0, '2021-08-22 14:03:06', '2021-08-22 12:03:06'),
(8951768, 331756031, 00042, 33175603100042, '2008-07-04', '2008-09-29', 'A', 1, '', '', '', 0, '', 0, '4781Z', 'NAFRev2', 1, 'N', 0, '2021-08-22 14:03:06', '2021-08-22 12:03:06'),
(8951769, 331756031, 00042, 33175603100042, '2007-07-31', '2008-07-03', 'F', 1, '', '', '', 0, '', 0, '526D', 'NAFRev1', 0, 'N', 0, '2021-08-22 14:03:06', '2021-08-22 12:03:06'),
(8951770, 331756031, 00042, 33175603100042, '2006-06-30', '2007-07-06', 'F', 1, '', '', '', 0, '', 0, '526D', 'NAFRev1', 0, 'N', 0, '2021-08-22 14:03:06', '2021-08-22 12:03:06'),
(8951771, 331756031, 00042, 33175603100042, '2006-03-08', '2006-06-29', 'A', 1, '', '', '', 0, '', 0, '526D', 'NAFRev1', 0, 'N', 0, '2021-08-22 14:03:06', '2021-08-22 12:03:06'),
(8951772, 331756031, 00042, 33175603100042, '2004-12-25', '2006-03-07', 'F', 0, '', '', '', 0, '', 0, '526D', 'NAFRev1', 1, 'N', 0, '2021-08-22 14:03:06', '2021-08-22 12:03:06'),
(8951773, 331756031, 00042, 33175603100042, '2004-11-12', '2004-12-24', 'F', 1, '', '', '', 0, '', 0, '', '', 0, 'N', 0, '2021-08-22 14:03:06', '2021-08-22 12:03:06'),
(8951774, 331756031, 00042, 33175603100042, '2004-09-14', '2004-11-11', 'A', 1, '', '', '', 0, '', 0, '', '', 0, 'N', 0, '2021-08-22 14:03:06', '2021-08-22 12:03:06'),
(8951775, 331756031, 00042, 33175603100042, '2004-03-19', '2004-09-13', 'F', 0, '', '', '', 0, '', 0, '', '', 0, 'N', 0, '2021-08-22 14:03:06', '2021-08-22 12:03:06'),
(8951776, 331756031, 00059, 33175603100059, '2013-07-01', '2016-11-10', 'A', 0, '', '', '', 0, '', 0, '4781Z', 'NAFRev2', 0, 'N', 0, '2021-08-22 14:03:06', '2021-08-22 12:03:06'),
(8951777, 331756031, 00059, 33175603100059, '2019-05-31', '2019-11-07', 'A', 0, '', '', '', 0, 'PROVENCE TAPENADE', 0, '4781Z', 'NAFRev2', 0, 'N', 1, '2021-08-22 14:03:06', '2021-08-22 12:03:06'),
(8951778, 331756031, 00059, 33175603100059, '2019-11-08', '0000-00-00', 'A', 0, '', '', '', 0, 'PROVENCE TAPENADE', 0, '4781Z', 'NAFRev2', 0, 'O', 1, '2021-08-22 14:03:06', '2021-08-22 12:03:06'),
(8951779, 331756031, 00059, 33175603100059, '2018-10-06', '2019-05-30', 'A', 0, '', '', '', 0, 'PROVENCE TAPENADE', 0, '4781Z', 'NAFRev2', 0, 'O', 1, '2021-08-22 14:03:06', '2021-08-22 12:03:06'),
(8951780, 331756031, 00059, 33175603100059, '2017-09-14', '2018-10-05', 'A', 0, '', '', '', 0, 'PROVENCE TAPENADE', 1, '4781Z', 'NAFRev2', 0, 'N', 0, '2021-08-22 14:03:06', '2021-08-22 12:03:06'),
(8951781, 331756031, 00059, 33175603100059, '2016-12-31', '2017-09-13', 'A', 0, '', '', '', 0, '', 0, '4781Z', 'NAFRev2', 0, 'N', 1, '2021-08-22 14:03:06', '2021-08-22 12:03:06'),
(8951782, 331756031, 00059, 33175603100059, '2016-11-11', '2016-12-30', 'A', 0, '', '', '', 0, '', 0, '4781Z', 'NAFRev2', 0, 'O', 1, '2021-08-22 14:03:06', '2021-08-22 12:03:06');

-- --------------------------------------------------------

--
-- Structure de la table `StockEtablissement_test`
--

CREATE TABLE `StockEtablissement_test` (
  `id` int(11) NOT NULL DEFAULT 0,
  `siren` int(9) UNSIGNED ZEROFILL NOT NULL,
  `nic` int(5) UNSIGNED ZEROFILL NOT NULL,
  `siret` bigint(14) UNSIGNED ZEROFILL NOT NULL,
  `dateDebut` date NOT NULL,
  `statutDiffusionEtablissement` tinyint(1) NOT NULL,
  `dateCreationEtablissement` date NOT NULL,
  `trancheEffectifsEtablissement` char(2) NOT NULL COMMENT 'Tranche d’effectif salarié de l’établissement',
  `anneeEffectifsEtablissement` smallint(6) NOT NULL COMMENT 'Année de la tranche d’effectif salarié de l’établissement',
  `activitePrincipaleRegistreMetiersEtablissement` char(6) NOT NULL COMMENT 'Code de l’activité exercée par l’artisan inscrit au registre des métiers',
  `dateDernierTraitementEtablissement` datetime NOT NULL COMMENT 'Date de la dernière mise à jour effectuée au répertoire Sirene sur l’établissement',
  `etablissementSiege` tinyint(1) NOT NULL COMMENT 'Indicatrice précisant si le Siret est celui de l’établissement siège ou non',
  `nombrePeriodesEtablissement` smallint(6) NOT NULL COMMENT 'Nombre de périodes dans la vie de l’établissement',
  `complementAdresseEtablissement` varchar(255) NOT NULL,
  `numeroVoieEtablissement` char(5) NOT NULL,
  `indiceRepetitionEtablissement` varchar(10) NOT NULL,
  `typeVoieEtablissement` varchar(20) NOT NULL,
  `libelleVoieEtablissement` varchar(120) NOT NULL,
  `codePostalEtablissement` varchar(5) NOT NULL,
  `libelleCommuneEtablissement` varchar(70) NOT NULL,
  `libelleCommuneEtrangerEtablissement` varchar(100) NOT NULL,
  `distributionSpecialeEtablissement` varchar(50) NOT NULL,
  `codeCommuneEtablissement` char(5) NOT NULL,
  `codeCedexEtablissement` char(9) NOT NULL,
  `libellePaysEtrangerEtablissement` varchar(50) NOT NULL,
  `codepaysetrangeretablissement` char(5) NOT NULL,
  `libelleCedexEtablissement` varchar(50) NOT NULL,
  `complementAdresse2Etablissement` varchar(38) NOT NULL,
  `numeroVoie2Etablissement` smallint(4) NOT NULL,
  `indiceRepetition2Etablissement` char(1) NOT NULL,
  `typeVoie2Etablissement` char(4) NOT NULL,
  `libelleVoie2Etablissement` varchar(100) NOT NULL,
  `codePostal2Etablissement` char(5) NOT NULL,
  `libelleCommune2Etablissement` varchar(100) NOT NULL,
  `libelleCommuneEtranger2Etablissement` varchar(100) NOT NULL,
  `distributionSpeciale2Etablissement` varchar(26) NOT NULL,
  `codeCommune2Etablissement` char(5) NOT NULL,
  `codeCedex2Etablissement` char(9) NOT NULL,
  `libelleCedex2Etablissement` varchar(100) NOT NULL,
  `codePaysEtranger2Etablissement` char(5) NOT NULL,
  `libellePaysEtranger2Etablissement` varchar(100) NOT NULL,
  `etatAdministratifEtablissement` char(1) NOT NULL,
  `enseigne1Etablissement` varchar(60) NOT NULL,
  `enseigne2Etablissement` varchar(60) NOT NULL,
  `enseigne3Etablissement` varchar(60) NOT NULL,
  `denominationUsuelleEtablissement` varchar(120) NOT NULL,
  `activitePrincipaleEtablissement` varchar(6) NOT NULL,
  `nomenclatureActivitePrincipaleEtablissement` varchar(10) NOT NULL,
  `caractereEmployeurEtablissement` set('O','N','''''','') NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `StockEtablissement_test`
--

INSERT INTO `StockEtablissement_test` (`id`, `siren`, `nic`, `siret`, `dateDebut`, `statutDiffusionEtablissement`, `dateCreationEtablissement`, `trancheEffectifsEtablissement`, `anneeEffectifsEtablissement`, `activitePrincipaleRegistreMetiersEtablissement`, `dateDernierTraitementEtablissement`, `etablissementSiege`, `nombrePeriodesEtablissement`, `complementAdresseEtablissement`, `numeroVoieEtablissement`, `indiceRepetitionEtablissement`, `typeVoieEtablissement`, `libelleVoieEtablissement`, `codePostalEtablissement`, `libelleCommuneEtablissement`, `libelleCommuneEtrangerEtablissement`, `distributionSpecialeEtablissement`, `codeCommuneEtablissement`, `codeCedexEtablissement`, `libellePaysEtrangerEtablissement`, `codepaysetrangeretablissement`, `libelleCedexEtablissement`, `complementAdresse2Etablissement`, `numeroVoie2Etablissement`, `indiceRepetition2Etablissement`, `typeVoie2Etablissement`, `libelleVoie2Etablissement`, `codePostal2Etablissement`, `libelleCommune2Etablissement`, `libelleCommuneEtranger2Etablissement`, `distributionSpeciale2Etablissement`, `codeCommune2Etablissement`, `codeCedex2Etablissement`, `libelleCedex2Etablissement`, `codePaysEtranger2Etablissement`, `libellePaysEtranger2Etablissement`, `etatAdministratifEtablissement`, `enseigne1Etablissement`, `enseigne2Etablissement`, `enseigne3Etablissement`, `denominationUsuelleEtablissement`, `activitePrincipaleEtablissement`, `nomenclatureActivitePrincipaleEtablissement`, `caractereEmployeurEtablissement`, `created_at`, `updated_at`) VALUES
(1112486, 306799107, 00017, 30679910700017, '2016-01-01', 1, '1900-01-01', '', 0, '', '2016-01-12 05:31:17', 1, 5, '', '164', '', 'RTE', 'DE PONT DE VEYLE', '01750', 'REPLONGES', '', '', '01320', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', 'F', '', '', '', '', '8621Z', 'NAFRev2', 'N', '2021-08-22 13:36:49', '0000-00-00 00:00:00'),
(1244084, 309007268, 00019, 30900726800019, '2017-12-31', 1, '1900-01-01', 'NN', 0, '', '2017-12-11 19:47:30', 1, 4, '', '24', '', 'RUE', 'PAUL LOUIS COURIER', '11100', 'NARBONNE', '', '', '11262', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', 'F', '', '', '', '', '8621Z', 'NAFRev2', 'N', '2021-08-22 13:36:49', '0000-00-00 00:00:00'),
(1270567, 309335412, 00024, 30933541200024, '1996-02-19', 1, '1900-01-01', 'NN', 0, '', '2019-11-14 14:00:39', 0, 4, '', '', '', 'RUE', 'DES PARCS DU BOURG', '44410', 'SAINT-LYPHARD', '', '', '44175', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', 'F', '', '', '', '', '851C', 'NAF1993', 'N', '2021-08-22 13:36:49', '0000-00-00 00:00:00'),
(1270568, 309335412, 00032, 30933541200032, '2007-12-15', 1, '1996-02-19', 'NN', 0, '', '2019-11-14 14:00:39', 1, 3, '', '4', '', 'RUE', 'DE BRETAGNE', '44410', 'SAINT-LYPHARD', '', '', '44175', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', 'F', '', '', '', '', '851C', 'NAFRev1', 'O', '2021-08-22 13:36:49', '0000-00-00 00:00:00'),
(1466698, 311768717, 00010, 31176871700010, '0000-00-00', 1, '0000-00-00', 'NN', 0, '', '2019-11-14 14:00:22', 1, 1, '', '', '', '', 'LA FONTAINE AUDON', '18240', 'SAINTE-GEMME-EN-SANCERROIS', '', '', '18208', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', 'F', '', '', '', '', '0117', 'NAP', 'N', '2021-08-22 13:36:49', '0000-00-00 00:00:00'),
(1612095, 313457244, 00017, 31345724400017, '1989-11-24', 1, '0000-00-00', 'NN', 0, '', '0000-00-00 00:00:00', 1, 1, '', '', '', '', '', '10300', 'MONTGUEUX', '', '', '10248', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', 'F', '', '', '', '', '8121', 'NAP', 'N', '2021-08-22 13:36:49', '0000-00-00 00:00:00'),
(3635306, 331756031, 00042, 33175603100042, '2013-07-01', 1, '2004-03-19', 'NN', 0, '', '2019-11-14 14:01:02', 0, 12, 'LE MONT MOUSSEAU BAT B2', '', '', 'AV', 'DOCTEUR MAZEN', '83500', 'LA SEYNE-SUR-MER', '', '', '83126', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', 'F', '', '', '', '', '4781Z', 'NAFRev2', 'N', '2021-08-22 13:36:49', '0000-00-00 00:00:00'),
(3635307, 331756031, 00059, 33175603100059, '2019-11-08', 1, '2013-07-01', 'NN', 0, '1039AP', '2019-11-14 14:01:02', 1, 7, 'RES OUTRE MER BAT B APP 102', '107', '', 'CHE', 'DE LA PERTUADE', '83140', 'SIX-FOURS-LES-PLAGES', '', '', '83129', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', 'A', '', '', '', 'PROVENCE TAPENADE', '4781Z', 'NAFRev2', 'O', '2021-08-22 13:36:49', '0000-00-00 00:00:00');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
