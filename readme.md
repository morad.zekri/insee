


## Install

to run the application please use this commands

    $ sudo docker-compose up -d
    $ sudo docker-compose exec php-fpm composer install
    $ sudo docker-compose exec php-fpm bin/console doctrine:migrations:execute --up 'DoctrineMigrations\Version20220419005249'  
    $ sudo docker-compose exec php-fpm bin/console doctrine:migrations:execute --up 'DoctrineMigrations\Version20220419010230' --em=insee

## Lunch App
    
to ensure that app work fine check first the status of message consumer :
        
    $ sudo docker-compose ps messages_consumer

after to lunch the async stock etablissement you can run this command :

    $ sudo docker-compose exec php-fpm bin/console app:insee-synchro

for the Api platform of stock Etablissment please visit :
`http://localhost:8080/api`

## PHP Insights

to analyze the code you can run this command

    $ sudo docker-compose exec php-fpm ./vendor/bin/phpinsights -s

## CS FIXER

to run the cs clean fix please use this commands

    $ sudo docker-compose exec php-fpm mkdir --parents tools/php-cs-fixer
    $ sudo docker-compose exec php-fpm composer require --working-dir=tools/php-cs-fixer friendsofphp/php-cs-fixer
    $ sudo docker-compose exec php-fpm tools/php-cs-fixer/vendor/bin/php-cs-fixer fix src
    $ sudo docker-compose exec php-fpm tools/php-cs-fixer/vendor/bin/php-cs-fixer fix tests

