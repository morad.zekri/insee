<?php

declare(strict_types=1);

namespace App\Application\Query;

final class StockEtablissementQuery implements Query
{
    public function __construct(
        private ?string $date = null,
        private int $nbByPage = 1000,
        private int $page = 1
    ) {
        if (!$date) {
            $this->date = date('Y-m-d', strtotime('-1 days'));
        }
    }

    public function getDate(): string
    {
        return $this->date;
    }

    public function getNbByPage(): int
    {
        return $this->nbByPage;
    }

    public function getPage(): int
    {
        return $this->page;
    }

    public function getDebut(): int
    {
        return $this->page * $this->nbByPage;
    }
}
