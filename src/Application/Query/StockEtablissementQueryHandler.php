<?php

declare(strict_types=1);

namespace App\Application\Query;

use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

final class StockEtablissementQueryHandler implements QueryHandler
{
    public function __construct(
        private HttpClientInterface $client,
    ) {
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function __invoke(StockEtablissementQuery $etablissementQuery)
    {
        $url = 'https://api.insee.fr/entreprises/sirene/V3/siret?q=dateDernierTraitementEtablissement:'
                .$etablissementQuery->getDate().
                '&debut='.$etablissementQuery->getDebut().
                '&nombre='.$etablissementQuery->getNbByPage().
                '&tri=true';

        try {
            $response = $this->client->request(
                'GET',
                $url,
                [
                    'headers' => [
                        'Accept' => 'application/json',
                        'Authorization' => 'Bearer 6c86736e-4ac3-3fff-8f85-5e258a9a38e8',
                    ],
                ]
            );

            return json_decode($response->getContent(), true)['etablissements'];
        } catch (\Exception $e) {
            throw new \Exception('erreur call api insee', $e->getMessage());
        }
    }
}
