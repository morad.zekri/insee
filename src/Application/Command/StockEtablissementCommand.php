<?php

declare(strict_types=1);

namespace App\Application\Command;

final class StockEtablissementCommand implements Command
{
    public function __construct(
        private array $etablissement
    ) {
    }

    public function getEtablissement(): array
    {
        return $this->etablissement;
    }
}
