<?php

declare(strict_types=1);

namespace App\Application\Command;

use Symfony\Component\Messenger\MessageBusInterface;

final class StockEtablissementCommandBus implements CommandBus
{
    public function __construct(
        private MessageBusInterface $commandBus
    ) {
    }

    public function dispatch(Command $command): void
    {
        $this->commandBus->dispatch($command);
    }
}
