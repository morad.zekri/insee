<?php

declare(strict_types=1);

namespace App\Application\Command;

final class StockEtablissementHistoriqueCommand implements Command
{
    public function __construct(
        private int $siren
    ) {
    }

    public function getSiren(): int
    {
        return $this->siren;
    }
}
