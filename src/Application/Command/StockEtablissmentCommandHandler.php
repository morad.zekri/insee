<?php

declare(strict_types=1);

namespace App\Application\Command;

use App\Domaine\DTO\StockEtablissementDTO;
use App\Infrastucture\Entity\WS\Stocketablissement;
use Doctrine\ORM\EntityManagerInterface;

final class StockEtablissmentCommandHandler implements CommandHandler
{
    public function __construct(
        private EntityManagerInterface $manager,
        private StockEtablissementHistoriqueCommandBus $commandBus
    ) {
    }

    public function __invoke(StockEtablissementCommand $etablissementMessage): void
    {
        $etablissement = $etablissementMessage->getEtablissement();

        /** @var Stocketablissement $stockEtablissment */
        $stockEtablissment = $this->manager->getRepository(Stocketablissement::class)->findOneBy([
            'siret' => $etablissement['siret'],
        ]);

        $stockEtablissment = StockEtablissementDTO::createFromArray($etablissement, $stockEtablissment);

        $this->manager->persist($stockEtablissment);
        $this->manager->flush();

        $this->commandBus->dispatch(new StockEtablissementHistoriqueCommand((int) $etablissement['siren']));
    }
}
