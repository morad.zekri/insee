<?php

declare(strict_types=1);

namespace App\Application\Command;

use App\Domaine\DTO\StockEtablissementHistoriqueDTO;
use App\Infrastucture\Entity\Insee\StocketablissementhistoriqueTest;
use App\Infrastucture\Entity\WS\Stocketablissement;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

final class StockEtablissementHistoriqueCommandHandler implements CommandHandler
{
    private $managerInsee;

    public function __construct(
        private EntityManagerInterface $manager,
        private ManagerRegistry $managerRegistry
    ) {
        $this->managerInsee = $this->managerRegistry->getManager('insee');
    }

    public function __invoke(StockEtablissementHistoriqueCommand $etablissementMessage): void
    {
        /** @var Stocketablissement $etablissement */
        $etablissement = $this->manager->getRepository(Stocketablissement::class)->findOneBy([
            'siren' => $etablissementMessage->getSiren(),
        ]);

        $etablissementHistorique = $this->managerInsee->getRepository(StocketablissementhistoriqueTest::class)->findOneBy([
            'siret' => $etablissement->getSiret(),
        ]);

        $etablissementHistorique = StockEtablissementHistoriqueDTO::createFromStockEtablissement(
            $etablissement,
            $etablissementHistorique
        );

        $this->managerInsee->persist($etablissementHistorique);
        $this->managerInsee->flush();
    }
}
