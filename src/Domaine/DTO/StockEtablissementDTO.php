<?php

declare(strict_types=1);

namespace App\Domaine\DTO;

use App\Infrastucture\Entity\WS\Stocketablissement;

final class StockEtablissementDTO
{
    public static function createFromArray(array $etablissement, ?Stocketablissement $stockEtablissement = null): Stocketablissement
    {
        if (!$stockEtablissement) {
            $stockEtablissement = new Stocketablissement();
        }

        $stockEtablissement->setSiren((int) $etablissement['siren']);
        $stockEtablissement->setSiret($etablissement['siret']);
        $stockEtablissement->setNic((int) $etablissement['nic']);
        $stockEtablissement->setStatutdiffusionetablissement((bool) $etablissement['statutDiffusionEtablissement']);
        $stockEtablissement->setDatecreationetablissement(new \DateTime($etablissement['dateCreationEtablissement']));
        $stockEtablissement->setTrancheeffectifsetablissement($etablissement['trancheEffectifsEtablissement'] ?? '');
        $stockEtablissement->setAnneeeffectifsetablissement((int) $etablissement['anneeEffectifsEtablissement'] ?? 0);
        $stockEtablissement->setActiviteprincipaleregistremetiersetablissement($etablissement['activitePrincipaleRegistreMetiersEtablissement'] ?? '');
        $stockEtablissement->setDatederniertraitementetablissement(new \DateTime($etablissement['dateDernierTraitementEtablissement']));
        $stockEtablissement->setEtablissementsiege($etablissement['etablissementSiege']);
        $stockEtablissement->setNombreperiodesetablissement((int) $etablissement['nombrePeriodesEtablissement']);
        self::adresseEtablissement($stockEtablissement, $etablissement['adresseEtablissement']);
        self::adresse2Etablissement($stockEtablissement, $etablissement['adresse2Etablissement']);
        self::periodesEtablissement($stockEtablissement, $etablissement['periodesEtablissement']);
        $stockEtablissement->setCreatedAt(new \DateTime());
        $stockEtablissement->setUpdatedAt(new \DateTime());

        return $stockEtablissement;
    }


    private static function adresseEtablissement(?Stocketablissement $stockEtablissement, $adresseEtablissement): void
    {
        $stockEtablissement->setComplementadresseetablissement((string) $adresseEtablissement['complementAdresseEtablissement'] ?? 0);
        $stockEtablissement->setNumerovoieetablissement($adresseEtablissement['numeroVoieEtablissement'] ?? '');
        $stockEtablissement->setIndicerepetitionetablissement($adresseEtablissement['indiceRepetitionEtablissement'] ?? '');
        $stockEtablissement->setTypevoieetablissement($adresseEtablissement['typeVoieEtablissement'] ?? '');
        $stockEtablissement->setLibellevoieetablissement($adresseEtablissement['libelleVoieEtablissement'] ?? '');
        $stockEtablissement->setCodepostaletablissement($adresseEtablissement['codePostalEtablissement'] ?? '');
        $stockEtablissement->setLibellecommuneetablissement($adresseEtablissement['libelleCommuneEtablissement'] ?? '');
        $stockEtablissement->setLibellecommuneetrangeretablissement($adresseEtablissement['libelleCommuneEtrangerEtablissement'] ?? '');
        $stockEtablissement->setDistributionspecialeetablissement($adresseEtablissement['distributionSpecialeEtablissement'] ?? '');
        $stockEtablissement->setCodecommuneetablissement($adresseEtablissement['codeCommuneEtablissement'] ?? '');
        $stockEtablissement->setCodecedexetablissement($adresseEtablissement['codeCedexEtablissement'] ?? '');
        $stockEtablissement->setLibellepaysetrangeretablissement($adresseEtablissement['libellePaysEtrangerEtablissement'] ?? '');
        $stockEtablissement->setCodepaysetrangeretablissement($adresseEtablissement['codepaysetrangeretablissement'] ?? '');
        $stockEtablissement->setLibellecedexetablissement($adresseEtablissement['libelleCedexEtablissement'] ?? '');
    }


    private static function adresse2Etablissement(?Stocketablissement $stockEtablissement, $adresseEtablissement): void
    {
        $stockEtablissement->setComplementadresse2etablissement($adresseEtablissement['complementAdresse2Etablissement'] ?? '');
        $stockEtablissement->setNumerovoie2etablissement((int) $adresseEtablissement['numeroVoie2Etablissement'] ?? 0);
        $stockEtablissement->setIndicerepetition2etablissement($adresseEtablissement['indiceRepetition2Etablissement'] ?? '');
        $stockEtablissement->setTypevoie2etablissement($adresseEtablissement['typeVoie2Etablissement'] ?? '');
        $stockEtablissement->setLibellevoie2etablissement($adresseEtablissement['libelleVoie2Etablissement'] ?? '');
        $stockEtablissement->setCodepostal2etablissement($adresseEtablissement['codePostal2Etablissement'] ?? '');
        $stockEtablissement->setLibellecommune2etablissement($adresseEtablissement['libelleCommune2Etablissement'] ?? '');
        $stockEtablissement->setLibellecommuneetranger2etablissement($adresseEtablissement['libelleCommuneEtranger2Etablissement'] ?? '');
        $stockEtablissement->setDistributionspeciale2etablissement($adresseEtablissement['distributionSpeciale2Etablissement'] ?? '');
        $stockEtablissement->setCodecommune2etablissement($adresseEtablissement['codeCommune2Etablissement'] ?? '');
        $stockEtablissement->setCodecedex2etablissement($adresseEtablissement['codeCedex2Etablissement'] ?? '');
        $stockEtablissement->setLibellecedex2etablissement($adresseEtablissement['libelleCedex2Etablissement'] ?? '');
        $stockEtablissement->setCodepaysetranger2etablissement($adresseEtablissement['codePaysEtranger2Etablissement'] ?? '');
        $stockEtablissement->setLibellepaysetranger2etablissement($adresseEtablissement['libellePaysEtranger2Etablissement'] ?? '');
    }


    private static function periodesEtablissement(?Stocketablissement $stockEtablissement, $periodesEtablissement): void
    {
        $stockEtablissement->setEtatadministratifetablissement($periodesEtablissement['etatAdministratifEtablissement'] ?? '');
        $stockEtablissement->setEnseigne1etablissement($periodesEtablissement['enseigne1Etablissement'] ?? '');
        $stockEtablissement->setEnseigne2etablissement($periodesEtablissement['enseigne2Etablissement'] ?? '');
        $stockEtablissement->setEnseigne3etablissement($periodesEtablissement['enseigne3Etablissement'] ?? '');
        $stockEtablissement->setDenominationusuelleetablissement($periodesEtablissement['denominationUsuelleEtablissement'] ?? '');
        $stockEtablissement->setActiviteprincipaleetablissement($periodesEtablissement['activitePrincipaleEtablissement'] ?? '');
        $stockEtablissement->setNomenclatureactiviteprincipaleetablissement($periodesEtablissement['nomenclatureActivitePrincipaleEtablissement'] ?? '');

        $stockEtablissement->setCaractereemployeuretablissement($periodesEtablissement['caractereEmployeurEtablissement'] ?? ['']);

        $stockEtablissement->setDatedebut(
            isset($etablissment['periodesEtablissement']['dateDebut'])
                ? new \DateTime($etablissment['periodesEtablissement']['dateDebut'])
                : new \DateTime()
        );
    }
}
