<?php

declare(strict_types=1);

namespace App\Domaine\DTO;

use App\Infrastucture\Entity\Insee\StocketablissementhistoriqueTest;
use App\Infrastucture\Entity\WS\Stocketablissement;

final class StockEtablissementHistoriqueDTO
{
    public static function createFromStockEtablissement(
        Stocketablissement $etablissement,
        ?StocketablissementhistoriqueTest $etablissementHistorique = null
    ) {
        if (!$etablissementHistorique) {
            $etablissementHistorique = new StocketablissementhistoriqueTest();
        }

        $etablissementHistorique->setSiren($etablissement->getSiren());
        $etablissementHistorique->setNic($etablissement->getNic());
        $etablissementHistorique->setSiret($etablissement->getSiret());
        $etablissementHistorique->setDatedebut($etablissement->getDateDebut());
        $etablissementHistorique->setEtatadministratifetablissement($etablissement->getEtatAdministratifEtablissement());

        self::setChangementEnseigneEtablissement($etablissementHistorique, $etablissement);
        self::setChangementDenomination($etablissementHistorique, $etablissement);

        $etablissementHistorique->setActiviteprincipaleetablissement($etablissement->getActivitePrincipaleEtablissement());
        $etablissementHistorique->setChangementactiviteprincipaleetablissement(false);
        $etablissementHistorique->setNomenclatureactiviteprincipaleetablissement($etablissement->getNomenclatureActivitePrincipaleEtablissement());
        $etablissementHistorique->setCaractereemployeuretablissement($etablissement->getCaractereEmployeurEtablissement());
        $etablissementHistorique->setChangementetatadministratifetablissement(false);
        $etablissementHistorique->setChangementcaractereemployeuretablissement(false);

        $etablissementHistorique->setCreatedAt(new \DateTime());
        $etablissementHistorique->setUpdatedAt(new \DateTime());
        $etablissementHistorique->setDatefin(new \DateTime());

        return $etablissementHistorique;
    }

    private static function setChangementEnseigneEtablissement(?StocketablissementhistoriqueTest $etablissementHistorique, Stocketablissement $etablissement): void
    {
        $etablissementHistorique->setEnseigne1etablissement($etablissement->getEnseigne1Etablissement());
        $etablissementHistorique->setEnseigne2etablissement($etablissement->getEnseigne2Etablissement());
        $etablissementHistorique->setEnseigne3etablissement($etablissement->getEnseigne3Etablissement());
        $etablissementHistorique->setChangementenseigneetablissement(false);
    }

    private static function setChangementDenomination(?StocketablissementhistoriqueTest $etablissementHistorique, Stocketablissement $etablissement): void
    {
        $etablissementHistorique->setDenominationusuelleetablissement($etablissement->getDenominationUsuelleEtablissement());
        $etablissementHistorique->setChangementdenominationusuelleetablissement(false);
    }
}
