<?php

declare(strict_types=1);

namespace App\Command;

use App\Application\Command\StockEtablissementCommand;
use App\Application\Command\StockEtablissementCommandBus;
use App\Application\Query\StockEtablissementQuery;
use App\Application\Query\StockEtablissementQueryBus;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:insee-synchro',
    description: 'Add a short description for your command',
)]
final class InseeSynchroCommand extends Command
{
    public function __construct(
        private StockEtablissementCommandBus $bus,
        private StockEtablissementQueryBus $queryBus
    ) {
        parent::__construct('app:insee-synchro');
    }

    protected function configure(): void
    {
        $this
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        for ($i = 0; $i <= 5; ++$i) {
            $etablissmentList = $this->queryBus->ask(new StockEtablissementQuery(page: $i));
            foreach ($etablissmentList as $etablissment) {
                $this->bus->dispatch(new StockEtablissementCommand($etablissment));
            }
        }

        $io->success('La synchro des etablissement est en cours en mode async');

        return Command::SUCCESS;
    }
}
