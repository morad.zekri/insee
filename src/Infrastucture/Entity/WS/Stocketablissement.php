<?php

declare(strict_types=1);

namespace App\Infrastucture\Entity\WS;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * Stocketablissement.
 *
 * @ORM\Table(name="StockEtablissement", uniqueConstraints={@ORM\UniqueConstraint(name="siret", columns={"siret"})}, indexes={@ORM\Index(name="code_commune", columns={"codeCommuneEtablissement"}), @ORM\Index(name="siren", columns={"siren", "nic"})})
 * @ORM\Entity
 */
#[ApiResource]
class Stocketablissement
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="siren", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $siren;

    /**
     * @var int
     *
     * @ORM\Column(name="nic", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $nic;

    /**
     * @var int
     *
     * @ORM\Column(name="siret", type="bigint", nullable=false, options={"unsigned"=true})
     */
    private $siret;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateDebut", type="date", nullable=false)
     */
    private $dateDebut;

    /**
     * @var bool
     *
     * @ORM\Column(name="statutDiffusionEtablissement", type="boolean", nullable=false)
     */
    private $statutDiffusionEtablissement;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateCreationEtablissement", type="date", nullable=false)
     */
    private $dateCreationEtablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="trancheEffectifsEtablissement", type="string", length=2, nullable=false, options={"fixed"=true,"comment"="Tranche d’effectif salarié de l’établissement"})
     */
    private $trancheEffectifsEtablissement;

    /**
     * @var int
     *
     * @ORM\Column(name="anneeEffectifsEtablissement", type="smallint", nullable=false, options={"comment"="Année de la tranche d’effectif salarié de l’établissement"})
     */
    private $anneeEffectifsEtablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="activitePrincipaleRegistreMetiersEtablissement", type="string", length=6, nullable=false, options={"fixed"=true,"comment"="Code de l’activité exercée par l’artisan inscrit au registre des métiers"})
     */
    private $activitePrincipaleRegistreMetiersEtablissement;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateDernierTraitementEtablissement", type="datetime", nullable=false, options={"comment"="Date de la dernière mise à jour effectuée au répertoire Sirene sur l’établissement"})
     */
    private $dateDernierTraitementEtablissement;

    /**
     * @var bool
     *
     * @ORM\Column(name="etablissementSiege", type="boolean", nullable=false, options={"comment"="Indicatrice précisant si le Siret est celui de l’établissement siège ou non"})
     */
    private $etablissementSiege;

    /**
     * @var int
     *
     * @ORM\Column(name="nombrePeriodesEtablissement", type="smallint", nullable=false, options={"comment"="Nombre de périodes dans la vie de l’établissement"})
     */
    private $nombrePeriodesEtablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="complementAdresseEtablissement", type="string", length=255, nullable=false)
     */
    private $complementAdresseEtablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="numeroVoieEtablissement", type="string", length=5, nullable=false, options={"fixed"=true})
     */
    private $numeroVoieEtablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="indiceRepetitionEtablissement", type="string", length=10, nullable=false)
     */
    private $indiceRepetitionEtablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="typeVoieEtablissement", type="string", length=20, nullable=false)
     */
    private $typeVoieEtablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="libelleVoieEtablissement", type="string", length=120, nullable=false)
     */
    private $libelleVoieEtablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="codePostalEtablissement", type="string", length=5, nullable=false)
     */
    private $codePostalEtablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="libelleCommuneEtablissement", type="string", length=70, nullable=false)
     */
    private $libelleCommuneEtablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="libelleCommuneEtrangerEtablissement", type="string", length=100, nullable=false)
     */
    private $libelleCommuneEtrangerEtablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="distributionSpecialeEtablissement", type="string", length=50, nullable=false)
     */
    private $distributionSpecialeEtablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="codeCommuneEtablissement", type="string", length=5, nullable=false, options={"fixed"=true})
     */
    private $codeCommuneEtablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="codeCedexEtablissement", type="string", length=9, nullable=false, options={"fixed"=true})
     */
    private $codeCedexEtablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="libellePaysEtrangerEtablissement", type="string", length=50, nullable=false)
     */
    private $libellePaysEtrangerEtablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="codePaysEtrangerEtablissement", type="string", length=5, nullable=false, options={"fixed"=true})
     */
    private $codePaysEtrangerEtablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="libelleCedexEtablissement", type="string", length=50, nullable=false)
     */
    private $libelleCedexEtablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="complementAdresse2Etablissement", type="string", length=38, nullable=false)
     */
    private $complementAdresse2Etablissement;

    /**
     * @var int
     *
     * @ORM\Column(name="numeroVoie2Etablissement", type="smallint", nullable=false)
     */
    private $numeroVoie2Etablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="indiceRepetition2Etablissement", type="string", length=1, nullable=false, options={"fixed"=true})
     */
    private $indiceRepetition2Etablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="typeVoie2Etablissement", type="string", length=4, nullable=false, options={"fixed"=true})
     */
    private $typeVoie2Etablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="libelleVoie2Etablissement", type="string", length=100, nullable=false)
     */
    private $libelleVoie2Etablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="codePostal2Etablissement", type="string", length=5, nullable=false, options={"fixed"=true})
     */
    private $codePostal2Etablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="libelleCommune2Etablissement", type="string", length=100, nullable=false)
     */
    private $libelleCommune2Etablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="libelleCommuneEtranger2Etablissement", type="string", length=100, nullable=false)
     */
    private $libelleCommuneEtranger2Etablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="distributionSpeciale2Etablissement", type="string", length=26, nullable=false)
     */
    private $distributionSpeciale2Etablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="codeCommune2Etablissement", type="string", length=5, nullable=false, options={"fixed"=true})
     */
    private $codeCommune2Etablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="codeCedex2Etablissement", type="string", length=9, nullable=false, options={"fixed"=true})
     */
    private $codeCedex2Etablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="libelleCedex2Etablissement", type="string", length=100, nullable=false)
     */
    private $libelleCedex2Etablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="codePaysEtranger2Etablissement", type="string", length=5, nullable=false, options={"fixed"=true})
     */
    private $codePaysEtranger2Etablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="libellePaysEtranger2Etablissement", type="string", length=100, nullable=false)
     */
    private $libellePaysEtranger2Etablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="etatAdministratifEtablissement", type="string", length=1, nullable=false, options={"fixed"=true})
     */
    private $etatAdministratifEtablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="enseigne1Etablissement", type="string", length=60, nullable=false)
     */
    private $enseigne1Etablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="enseigne2Etablissement", type="string", length=60, nullable=false)
     */
    private $enseigne2Etablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="enseigne3Etablissement", type="string", length=60, nullable=false)
     */
    private $enseigne3Etablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="denominationUsuelleEtablissement", type="string", length=120, nullable=false)
     */
    private $denominationUsuelleEtablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="activitePrincipaleEtablissement", type="string", length=6, nullable=false)
     */
    private $activitePrincipaleEtablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="nomenclatureActivitePrincipaleEtablissement", type="string", length=10, nullable=false)
     */
    private $nomenclatureActivitePrincipaleEtablissement;

    /**
     * @var array
     *
     * @ORM\Column(name="caractereEmployeurEtablissement", type="simple_array", length=0, nullable=false)
     */
    private $caractereEmployeurEtablissement;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false, options={"default"="current_timestamp()"})
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false, options={"default"="'0000-00-00 00:00:00'"})
     */
    private $updatedAt;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSiren(): ?int
    {
        return $this->siren;
    }

    public function setSiren(int $siren): self
    {
        $this->siren = (int) $siren;

        return $this;
    }

    public function getNic(): ?int
    {
        return $this->nic;
    }

    public function setNic(int $nic): self
    {
        $this->nic = $nic;

        return $this;
    }

    public function getSiret(): ?string
    {
        return $this->siret;
    }

    public function setSiret(string $siret): self
    {
        $this->siret = $siret;

        return $this;
    }

    public function getDateDebut(): ?\DateTimeInterface
    {
        return $this->dateDebut;
    }

    public function setDateDebut(\DateTimeInterface $dateDebut): self
    {
        $this->dateDebut = $dateDebut;

        return $this;
    }

    public function getStatutDiffusionEtablissement(): ?bool
    {
        return $this->statutDiffusionEtablissement;
    }

    public function setStatutDiffusionEtablissement(bool $statutDiffusionEtablissement): self
    {
        $this->statutDiffusionEtablissement = $statutDiffusionEtablissement;

        return $this;
    }

    public function getDateCreationEtablissement(): ?\DateTimeInterface
    {
        return $this->dateCreationEtablissement;
    }

    public function setDateCreationEtablissement(\DateTimeInterface $dateCreationEtablissement): self
    {
        $this->dateCreationEtablissement = $dateCreationEtablissement;

        return $this;
    }

    public function getTrancheEffectifsEtablissement(): ?string
    {
        return $this->trancheEffectifsEtablissement;
    }

    public function setTrancheEffectifsEtablissement(string $trancheEffectifsEtablissement): self
    {
        $this->trancheEffectifsEtablissement = $trancheEffectifsEtablissement;

        return $this;
    }

    public function getAnneeEffectifsEtablissement(): ?int
    {
        return $this->anneeEffectifsEtablissement;
    }

    public function setAnneeEffectifsEtablissement(int $anneeEffectifsEtablissement): self
    {
        $this->anneeEffectifsEtablissement = $anneeEffectifsEtablissement;

        return $this;
    }

    public function getActivitePrincipaleRegistreMetiersEtablissement(): ?string
    {
        return $this->activitePrincipaleRegistreMetiersEtablissement;
    }

    public function setActivitePrincipaleRegistreMetiersEtablissement(string $activitePrincipaleRegistreMetiersEtablissement): self
    {
        $this->activitePrincipaleRegistreMetiersEtablissement = $activitePrincipaleRegistreMetiersEtablissement;

        return $this;
    }

    public function getDateDernierTraitementEtablissement(): ?\DateTimeInterface
    {
        return $this->dateDernierTraitementEtablissement;
    }

    public function setDateDernierTraitementEtablissement(\DateTimeInterface $dateDernierTraitementEtablissement): self
    {
        $this->dateDernierTraitementEtablissement = $dateDernierTraitementEtablissement;

        return $this;
    }

    public function getEtablissementSiege(): ?bool
    {
        return $this->etablissementSiege;
    }

    public function setEtablissementSiege(bool $etablissementSiege): self
    {
        $this->etablissementSiege = $etablissementSiege;

        return $this;
    }

    public function getNombrePeriodesEtablissement(): ?int
    {
        return $this->nombrePeriodesEtablissement;
    }

    public function setNombrePeriodesEtablissement(int $nombrePeriodesEtablissement): self
    {
        $this->nombrePeriodesEtablissement = $nombrePeriodesEtablissement;

        return $this;
    }

    public function getComplementAdresseEtablissement(): ?string
    {
        return $this->complementAdresseEtablissement;
    }

    public function setComplementAdresseEtablissement(string $complementAdresseEtablissement): self
    {
        $this->complementAdresseEtablissement = $complementAdresseEtablissement;

        return $this;
    }

    public function getNumeroVoieEtablissement(): ?string
    {
        return $this->numeroVoieEtablissement;
    }

    public function setNumeroVoieEtablissement(string $numeroVoieEtablissement): self
    {
        $this->numeroVoieEtablissement = $numeroVoieEtablissement;

        return $this;
    }

    public function getIndiceRepetitionEtablissement(): ?string
    {
        return $this->indiceRepetitionEtablissement;
    }

    public function setIndiceRepetitionEtablissement(string $indiceRepetitionEtablissement): self
    {
        $this->indiceRepetitionEtablissement = $indiceRepetitionEtablissement;

        return $this;
    }

    public function getTypeVoieEtablissement(): ?string
    {
        return $this->typeVoieEtablissement;
    }

    public function setTypeVoieEtablissement(string $typeVoieEtablissement): self
    {
        $this->typeVoieEtablissement = $typeVoieEtablissement;

        return $this;
    }

    public function getLibelleVoieEtablissement(): ?string
    {
        return $this->libelleVoieEtablissement;
    }

    public function setLibelleVoieEtablissement(string $libelleVoieEtablissement): self
    {
        $this->libelleVoieEtablissement = $libelleVoieEtablissement;

        return $this;
    }

    public function getCodePostalEtablissement(): ?string
    {
        return $this->codePostalEtablissement;
    }

    public function setCodePostalEtablissement(string $codePostalEtablissement): self
    {
        $this->codePostalEtablissement = $codePostalEtablissement;

        return $this;
    }

    public function getLibelleCommuneEtablissement(): ?string
    {
        return $this->libelleCommuneEtablissement;
    }

    public function setLibelleCommuneEtablissement(string $libelleCommuneEtablissement): self
    {
        $this->libelleCommuneEtablissement = $libelleCommuneEtablissement;

        return $this;
    }

    public function getLibelleCommuneEtrangerEtablissement(): ?string
    {
        return $this->libelleCommuneEtrangerEtablissement;
    }

    public function setLibelleCommuneEtrangerEtablissement(string $libelleCommuneEtrangerEtablissement): self
    {
        $this->libelleCommuneEtrangerEtablissement = $libelleCommuneEtrangerEtablissement;

        return $this;
    }

    public function getDistributionSpecialeEtablissement(): ?string
    {
        return $this->distributionSpecialeEtablissement;
    }

    public function setDistributionSpecialeEtablissement(string $distributionSpecialeEtablissement): self
    {
        $this->distributionSpecialeEtablissement = $distributionSpecialeEtablissement;

        return $this;
    }

    public function getCodeCommuneEtablissement(): ?string
    {
        return $this->codeCommuneEtablissement;
    }

    public function setCodeCommuneEtablissement(string $codeCommuneEtablissement): self
    {
        $this->codeCommuneEtablissement = $codeCommuneEtablissement;

        return $this;
    }

    public function getCodeCedexEtablissement(): ?string
    {
        return $this->codeCedexEtablissement;
    }

    public function setCodeCedexEtablissement(string $codeCedexEtablissement): self
    {
        $this->codeCedexEtablissement = $codeCedexEtablissement;

        return $this;
    }

    public function getLibellePaysEtrangerEtablissement(): ?string
    {
        return $this->libellePaysEtrangerEtablissement;
    }

    public function setLibellePaysEtrangerEtablissement(string $libellePaysEtrangerEtablissement): self
    {
        $this->libellePaysEtrangerEtablissement = $libellePaysEtrangerEtablissement;

        return $this;
    }

    public function getCodePaysEtrangerEtablissement(): ?string
    {
        return $this->codePaysEtrangerEtablissement;
    }

    public function setCodePaysEtrangerEtablissement(string $codePaysEtrangerEtablissement): self
    {
        $this->codePaysEtrangerEtablissement = $codePaysEtrangerEtablissement;

        return $this;
    }

    public function getLibelleCedexEtablissement(): ?string
    {
        return $this->libelleCedexEtablissement;
    }

    public function setLibelleCedexEtablissement(string $libelleCedexEtablissement): self
    {
        $this->libelleCedexEtablissement = $libelleCedexEtablissement;

        return $this;
    }

    public function getComplementAdresse2Etablissement(): ?string
    {
        return $this->complementAdresse2Etablissement;
    }

    public function setComplementAdresse2Etablissement(string $complementAdresse2Etablissement): self
    {
        $this->complementAdresse2Etablissement = $complementAdresse2Etablissement;

        return $this;
    }

    public function getNumeroVoie2Etablissement(): ?int
    {
        return $this->numeroVoie2Etablissement;
    }

    public function setNumeroVoie2Etablissement(int $numeroVoie2Etablissement): self
    {
        $this->numeroVoie2Etablissement = $numeroVoie2Etablissement;

        return $this;
    }

    public function getIndiceRepetition2Etablissement(): ?string
    {
        return $this->indiceRepetition2Etablissement;
    }

    public function setIndiceRepetition2Etablissement(string $indiceRepetition2Etablissement): self
    {
        $this->indiceRepetition2Etablissement = $indiceRepetition2Etablissement;

        return $this;
    }

    public function getTypeVoie2Etablissement(): ?string
    {
        return $this->typeVoie2Etablissement;
    }

    public function setTypeVoie2Etablissement(string $typeVoie2Etablissement): self
    {
        $this->typeVoie2Etablissement = $typeVoie2Etablissement;

        return $this;
    }

    public function getLibelleVoie2Etablissement(): ?string
    {
        return $this->libelleVoie2Etablissement;
    }

    public function setLibelleVoie2Etablissement(string $libelleVoie2Etablissement): self
    {
        $this->libelleVoie2Etablissement = $libelleVoie2Etablissement;

        return $this;
    }

    public function getCodePostal2Etablissement(): ?string
    {
        return $this->codePostal2Etablissement;
    }

    public function setCodePostal2Etablissement(string $codePostal2Etablissement): self
    {
        $this->codePostal2Etablissement = $codePostal2Etablissement;

        return $this;
    }

    public function getLibelleCommune2Etablissement(): ?string
    {
        return $this->libelleCommune2Etablissement;
    }

    public function setLibelleCommune2Etablissement(string $libelleCommune2Etablissement): self
    {
        $this->libelleCommune2Etablissement = $libelleCommune2Etablissement;

        return $this;
    }

    public function getLibelleCommuneEtranger2Etablissement(): ?string
    {
        return $this->libelleCommuneEtranger2Etablissement;
    }

    public function setLibelleCommuneEtranger2Etablissement(string $libelleCommuneEtranger2Etablissement): self
    {
        $this->libelleCommuneEtranger2Etablissement = $libelleCommuneEtranger2Etablissement;

        return $this;
    }

    public function getDistributionSpeciale2Etablissement(): ?string
    {
        return $this->distributionSpeciale2Etablissement;
    }

    public function setDistributionSpeciale2Etablissement(string $distributionSpeciale2Etablissement): self
    {
        $this->distributionSpeciale2Etablissement = $distributionSpeciale2Etablissement;

        return $this;
    }

    public function getCodeCommune2Etablissement(): ?string
    {
        return $this->codeCommune2Etablissement;
    }

    public function setCodeCommune2Etablissement(string $codeCommune2Etablissement): self
    {
        $this->codeCommune2Etablissement = $codeCommune2Etablissement;

        return $this;
    }

    public function getCodeCedex2Etablissement(): ?string
    {
        return $this->codeCedex2Etablissement;
    }

    public function setCodeCedex2Etablissement(string $codeCedex2Etablissement): self
    {
        $this->codeCedex2Etablissement = $codeCedex2Etablissement;

        return $this;
    }

    public function getLibelleCedex2Etablissement(): ?string
    {
        return $this->libelleCedex2Etablissement;
    }

    public function setLibelleCedex2Etablissement(string $libelleCedex2Etablissement): self
    {
        $this->libelleCedex2Etablissement = $libelleCedex2Etablissement;

        return $this;
    }

    public function getCodePaysEtranger2Etablissement(): ?string
    {
        return $this->codePaysEtranger2Etablissement;
    }

    public function setCodePaysEtranger2Etablissement(string $codePaysEtranger2Etablissement): self
    {
        $this->codePaysEtranger2Etablissement = $codePaysEtranger2Etablissement;

        return $this;
    }

    public function getLibellePaysEtranger2Etablissement(): ?string
    {
        return $this->libellePaysEtranger2Etablissement;
    }

    public function setLibellePaysEtranger2Etablissement(string $libellePaysEtranger2Etablissement): self
    {
        $this->libellePaysEtranger2Etablissement = $libellePaysEtranger2Etablissement;

        return $this;
    }

    public function getEtatAdministratifEtablissement(): ?string
    {
        return $this->etatAdministratifEtablissement;
    }

    public function setEtatAdministratifEtablissement(string $etatAdministratifEtablissement): self
    {
        $this->etatAdministratifEtablissement = $etatAdministratifEtablissement;

        return $this;
    }

    public function getEnseigne1Etablissement(): ?string
    {
        return $this->enseigne1Etablissement;
    }

    public function setEnseigne1Etablissement(string $enseigne1Etablissement): self
    {
        $this->enseigne1Etablissement = $enseigne1Etablissement;

        return $this;
    }

    public function getEnseigne2Etablissement(): ?string
    {
        return $this->enseigne2Etablissement;
    }

    public function setEnseigne2Etablissement(string $enseigne2Etablissement): self
    {
        $this->enseigne2Etablissement = $enseigne2Etablissement;

        return $this;
    }

    public function getEnseigne3Etablissement(): ?string
    {
        return $this->enseigne3Etablissement;
    }

    public function setEnseigne3Etablissement(string $enseigne3Etablissement): self
    {
        $this->enseigne3Etablissement = $enseigne3Etablissement;

        return $this;
    }

    public function getDenominationUsuelleEtablissement(): ?string
    {
        return $this->denominationUsuelleEtablissement;
    }

    public function setDenominationUsuelleEtablissement(string $denominationUsuelleEtablissement): self
    {
        $this->denominationUsuelleEtablissement = $denominationUsuelleEtablissement;

        return $this;
    }

    public function getActivitePrincipaleEtablissement(): ?string
    {
        return $this->activitePrincipaleEtablissement;
    }

    public function setActivitePrincipaleEtablissement(string $activitePrincipaleEtablissement): self
    {
        $this->activitePrincipaleEtablissement = $activitePrincipaleEtablissement;

        return $this;
    }

    public function getNomenclatureActivitePrincipaleEtablissement(): ?string
    {
        return $this->nomenclatureActivitePrincipaleEtablissement;
    }

    public function setNomenclatureActivitePrincipaleEtablissement(string $nomenclatureActivitePrincipaleEtablissement): self
    {
        $this->nomenclatureActivitePrincipaleEtablissement = $nomenclatureActivitePrincipaleEtablissement;

        return $this;
    }

    public function getCaractereEmployeurEtablissement(): ?array
    {
        return $this->caractereEmployeurEtablissement;
    }

    /**
     * @return $this
     */
    public function setCaractereEmployeurEtablissement(array $caractereEmployeurEtablissement): self
    {
        $this->caractereEmployeurEtablissement = $caractereEmployeurEtablissement;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}
