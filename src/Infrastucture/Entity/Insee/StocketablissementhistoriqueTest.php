<?php

declare(strict_types=1);

namespace App\Infrastucture\Entity\Insee;

use Doctrine\ORM\Mapping as ORM;

/**
 * StocketablissementhistoriqueTest.
 *
 * @ORM\Table(name="StockEtablissementHistorique_test")
 * @ORM\Entity
 */
class StocketablissementhistoriqueTest
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="siren", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $siren;

    /**
     * @var int
     *
     * @ORM\Column(name="nic", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $nic;

    /**
     * @var int
     *
     * @ORM\Column(name="siret", type="bigint", nullable=false, options={"unsigned"=true})
     */
    private $siret;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateDebut", type="date", nullable=false)
     */
    private $datedebut;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateFin", type="date", nullable=false)
     */
    private $datefin;

    /**
     * @var string
     *
     * @ORM\Column(name="etatAdministratifEtablissement", type="string", length=1, nullable=false, options={"fixed"=true})
     */
    private $etatadministratifetablissement;

    /**
     * @var bool
     *
     * @ORM\Column(name="changementEtatAdministratifEtablissement", type="boolean", nullable=false)
     */
    private $changementetatadministratifetablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="enseigne1Etablissement", type="string", length=55, nullable=false)
     */
    private $enseigne1etablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="enseigne2Etablissement", type="string", length=55, nullable=false)
     */
    private $enseigne2etablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="enseigne3Etablissement", type="string", length=55, nullable=false)
     */
    private $enseigne3etablissement;

    /**
     * @var bool
     *
     * @ORM\Column(name="changementEnseigneEtablissement", type="boolean", nullable=false)
     */
    private $changementenseigneetablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="denominationUsuelleEtablissement", type="string", length=110, nullable=false)
     */
    private $denominationusuelleetablissement;

    /**
     * @var bool
     *
     * @ORM\Column(name="changementDenominationUsuelleEtablissement", type="boolean", nullable=false)
     */
    private $changementdenominationusuelleetablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="activitePrincipaleEtablissement", type="string", length=6, nullable=false, options={"fixed"=true})
     */
    private $activiteprincipaleetablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="nomenclatureActivitePrincipaleEtablissement", type="string", length=8, nullable=false, options={"fixed"=true})
     */
    private $nomenclatureactiviteprincipaleetablissement;

    /**
     * @var bool
     *
     * @ORM\Column(name="changementActivitePrincipaleEtablissement", type="boolean", nullable=false)
     */
    private $changementactiviteprincipaleetablissement;

    /**
     * @var array
     *
     * @ORM\Column(name="caractereEmployeurEtablissement", type="simple_array", length=0, nullable=false)
     */
    private $caractereemployeuretablissement;

    /**
     * @var bool
     *
     * @ORM\Column(name="changementCaractereEmployeurEtablissement", type="boolean", nullable=false)
     */
    private $changementcaractereemployeuretablissement;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt = 'current_timestamp()';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    private $updatedAt = 'current_timestamp()';

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSiren(): ?int
    {
        return $this->siren;
    }

    public function setSiren(int $siren): self
    {
        $this->siren = $siren;

        return $this;
    }

    public function getNic(): ?int
    {
        return $this->nic;
    }

    public function setNic(int $nic): self
    {
        $this->nic = $nic;

        return $this;
    }

    public function getSiret(): ?string
    {
        return $this->siret;
    }

    public function setSiret(string $siret): self
    {
        $this->siret = $siret;

        return $this;
    }

    public function getDatedebut(): ?\DateTimeInterface
    {
        return $this->datedebut;
    }

    public function setDatedebut(\DateTimeInterface $datedebut): self
    {
        $this->datedebut = $datedebut;

        return $this;
    }

    public function getDatefin(): ?\DateTimeInterface
    {
        return $this->datefin;
    }

    public function setDatefin(\DateTimeInterface $datefin): self
    {
        $this->datefin = $datefin;

        return $this;
    }

    public function getEtatadministratifetablissement(): ?string
    {
        return $this->etatadministratifetablissement;
    }

    public function setEtatadministratifetablissement(string $etatadministratifetablissement): self
    {
        $this->etatadministratifetablissement = $etatadministratifetablissement;

        return $this;
    }

    public function getChangementetatadministratifetablissement(): ?bool
    {
        return $this->changementetatadministratifetablissement;
    }

    public function setChangementetatadministratifetablissement(bool $changementetatadministratifetablissement): self
    {
        $this->changementetatadministratifetablissement = $changementetatadministratifetablissement;

        return $this;
    }

    public function getEnseigne1etablissement(): ?string
    {
        return $this->enseigne1etablissement;
    }

    public function setEnseigne1etablissement(string $enseigne1etablissement): self
    {
        $this->enseigne1etablissement = $enseigne1etablissement;

        return $this;
    }

    public function getEnseigne2etablissement(): ?string
    {
        return $this->enseigne2etablissement;
    }

    public function setEnseigne2etablissement(string $enseigne2etablissement): self
    {
        $this->enseigne2etablissement = $enseigne2etablissement;

        return $this;
    }

    public function getEnseigne3etablissement(): ?string
    {
        return $this->enseigne3etablissement;
    }

    public function setEnseigne3etablissement(string $enseigne3etablissement): self
    {
        $this->enseigne3etablissement = $enseigne3etablissement;

        return $this;
    }

    public function getChangementenseigneetablissement(): ?bool
    {
        return $this->changementenseigneetablissement;
    }

    public function setChangementenseigneetablissement(bool $changementenseigneetablissement): self
    {
        $this->changementenseigneetablissement = $changementenseigneetablissement;

        return $this;
    }

    public function getDenominationusuelleetablissement(): ?string
    {
        return $this->denominationusuelleetablissement;
    }

    public function setDenominationusuelleetablissement(string $denominationusuelleetablissement): self
    {
        $this->denominationusuelleetablissement = $denominationusuelleetablissement;

        return $this;
    }

    public function getChangementdenominationusuelleetablissement(): ?bool
    {
        return $this->changementdenominationusuelleetablissement;
    }

    public function setChangementdenominationusuelleetablissement(bool $changementdenominationusuelleetablissement): self
    {
        $this->changementdenominationusuelleetablissement = $changementdenominationusuelleetablissement;

        return $this;
    }

    public function getActiviteprincipaleetablissement(): ?string
    {
        return $this->activiteprincipaleetablissement;
    }

    public function setActiviteprincipaleetablissement(string $activiteprincipaleetablissement): self
    {
        $this->activiteprincipaleetablissement = $activiteprincipaleetablissement;

        return $this;
    }

    public function getNomenclatureactiviteprincipaleetablissement(): ?string
    {
        return $this->nomenclatureactiviteprincipaleetablissement;
    }

    public function setNomenclatureactiviteprincipaleetablissement(string $nomenclatureactiviteprincipaleetablissement): self
    {
        $this->nomenclatureactiviteprincipaleetablissement = $nomenclatureactiviteprincipaleetablissement;

        return $this;
    }

    public function getChangementactiviteprincipaleetablissement(): ?bool
    {
        return $this->changementactiviteprincipaleetablissement;
    }

    public function setChangementactiviteprincipaleetablissement(bool $changementactiviteprincipaleetablissement): self
    {
        $this->changementactiviteprincipaleetablissement = $changementactiviteprincipaleetablissement;

        return $this;
    }

    public function getCaractereemployeuretablissement(): ?array
    {
        return $this->caractereemployeuretablissement;
    }

    public function setCaractereemployeuretablissement(array $caractereemployeuretablissement): self
    {
        $this->caractereemployeuretablissement = $caractereemployeuretablissement;

        return $this;
    }

    public function getChangementcaractereemployeuretablissement(): ?bool
    {
        return $this->changementcaractereemployeuretablissement;
    }

    public function setChangementcaractereemployeuretablissement(bool $changementcaractereemployeuretablissement): self
    {
        $this->changementcaractereemployeuretablissement = $changementcaractereemployeuretablissement;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}
