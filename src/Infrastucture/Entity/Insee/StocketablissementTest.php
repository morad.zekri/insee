<?php

declare(strict_types=1);

namespace App\Infrastucture\Entity\Insee;

use Doctrine\ORM\Mapping as ORM;

/**
 * StocketablissementTest.
 *
 * @ORM\Table(name="StockEtablissement_test")
 * @ORM\Entity
 */
class StocketablissementTest
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="siren", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $siren;

    /**
     * @var int
     *
     * @ORM\Column(name="nic", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $nic;

    /**
     * @var int
     *
     * @ORM\Column(name="siret", type="bigint", nullable=false, options={"unsigned"=true})
     */
    private $siret;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateDebut", type="date", nullable=false)
     */
    private $datedebut;

    /**
     * @var bool
     *
     * @ORM\Column(name="statutDiffusionEtablissement", type="boolean", nullable=false)
     */
    private $statutdiffusionetablissement;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateCreationEtablissement", type="date", nullable=false)
     */
    private $datecreationetablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="trancheEffectifsEtablissement", type="string", length=2, nullable=false, options={"fixed"=true,"comment"="Tranche d’effectif salarié de l’établissement"})
     */
    private $trancheeffectifsetablissement;

    /**
     * @var int
     *
     * @ORM\Column(name="anneeEffectifsEtablissement", type="smallint", nullable=false, options={"comment"="Année de la tranche d’effectif salarié de l’établissement"})
     */
    private $anneeeffectifsetablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="activitePrincipaleRegistreMetiersEtablissement", type="string", length=6, nullable=false, options={"fixed"=true,"comment"="Code de l’activité exercée par l’artisan inscrit au registre des métiers"})
     */
    private $activiteprincipaleregistremetiersetablissement;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateDernierTraitementEtablissement", type="datetime", nullable=false, options={"comment"="Date de la dernière mise à jour effectuée au répertoire Sirene sur l’établissement"})
     */
    private $datederniertraitementetablissement;

    /**
     * @var bool
     *
     * @ORM\Column(name="etablissementSiege", type="boolean", nullable=false, options={"comment"="Indicatrice précisant si le Siret est celui de l’établissement siège ou non"})
     */
    private $etablissementsiege;

    /**
     * @var int
     *
     * @ORM\Column(name="nombrePeriodesEtablissement", type="smallint", nullable=false, options={"comment"="Nombre de périodes dans la vie de l’établissement"})
     */
    private $nombreperiodesetablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="complementAdresseEtablissement", type="string", length=255, nullable=false)
     */
    private $complementadresseetablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="numeroVoieEtablissement", type="string", length=5, nullable=false, options={"fixed"=true})
     */
    private $numerovoieetablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="indiceRepetitionEtablissement", type="string", length=10, nullable=false)
     */
    private $indicerepetitionetablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="typeVoieEtablissement", type="string", length=20, nullable=false)
     */
    private $typevoieetablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="libelleVoieEtablissement", type="string", length=120, nullable=false)
     */
    private $libellevoieetablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="codePostalEtablissement", type="string", length=5, nullable=false)
     */
    private $codepostaletablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="libelleCommuneEtablissement", type="string", length=70, nullable=false)
     */
    private $libellecommuneetablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="libelleCommuneEtrangerEtablissement", type="string", length=100, nullable=false)
     */
    private $libellecommuneetrangeretablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="distributionSpecialeEtablissement", type="string", length=50, nullable=false)
     */
    private $distributionspecialeetablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="codeCommuneEtablissement", type="string", length=5, nullable=false, options={"fixed"=true})
     */
    private $codecommuneetablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="codeCedexEtablissement", type="string", length=9, nullable=false, options={"fixed"=true})
     */
    private $codecedexetablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="libellePaysEtrangerEtablissement", type="string", length=50, nullable=false)
     */
    private $libellepaysetrangeretablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="codepaysetrangeretablissement", type="string", length=5, nullable=false, options={"fixed"=true})
     */
    private $codepaysetrangeretablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="libelleCedexEtablissement", type="string", length=50, nullable=false)
     */
    private $libellecedexetablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="complementAdresse2Etablissement", type="string", length=38, nullable=false)
     */
    private $complementadresse2etablissement;

    /**
     * @var int
     *
     * @ORM\Column(name="numeroVoie2Etablissement", type="smallint", nullable=false)
     */
    private $numerovoie2etablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="indiceRepetition2Etablissement", type="string", length=1, nullable=false, options={"fixed"=true})
     */
    private $indicerepetition2etablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="typeVoie2Etablissement", type="string", length=4, nullable=false, options={"fixed"=true})
     */
    private $typevoie2etablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="libelleVoie2Etablissement", type="string", length=100, nullable=false)
     */
    private $libellevoie2etablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="codePostal2Etablissement", type="string", length=5, nullable=false, options={"fixed"=true})
     */
    private $codepostal2etablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="libelleCommune2Etablissement", type="string", length=100, nullable=false)
     */
    private $libellecommune2etablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="libelleCommuneEtranger2Etablissement", type="string", length=100, nullable=false)
     */
    private $libellecommuneetranger2etablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="distributionSpeciale2Etablissement", type="string", length=26, nullable=false)
     */
    private $distributionspeciale2etablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="codeCommune2Etablissement", type="string", length=5, nullable=false, options={"fixed"=true})
     */
    private $codecommune2etablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="codeCedex2Etablissement", type="string", length=9, nullable=false, options={"fixed"=true})
     */
    private $codecedex2etablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="libelleCedex2Etablissement", type="string", length=100, nullable=false)
     */
    private $libellecedex2etablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="codePaysEtranger2Etablissement", type="string", length=5, nullable=false, options={"fixed"=true})
     */
    private $codepaysetranger2etablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="libellePaysEtranger2Etablissement", type="string", length=100, nullable=false)
     */
    private $libellepaysetranger2etablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="etatAdministratifEtablissement", type="string", length=1, nullable=false, options={"fixed"=true})
     */
    private $etatadministratifetablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="enseigne1Etablissement", type="string", length=60, nullable=false)
     */
    private $enseigne1etablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="enseigne2Etablissement", type="string", length=60, nullable=false)
     */
    private $enseigne2etablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="enseigne3Etablissement", type="string", length=60, nullable=false)
     */
    private $enseigne3etablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="denominationUsuelleEtablissement", type="string", length=120, nullable=false)
     */
    private $denominationusuelleetablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="activitePrincipaleEtablissement", type="string", length=6, nullable=false)
     */
    private $activiteprincipaleetablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="nomenclatureActivitePrincipaleEtablissement", type="string", length=10, nullable=false)
     */
    private $nomenclatureactiviteprincipaleetablissement;

    /**
     * @var array
     *
     * @ORM\Column(name="caractereEmployeurEtablissement", type="simple_array", length=0, nullable=false)
     */
    private $caractereemployeuretablissement;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false, options={"default"="current_timestamp()"})
     */
    private $createdAt = 'current_timestamp()';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false, options={"default"="'0000-00-00 00:00:00'"})
     */
    private $updatedAt = '\'0000-00-00 00:00:00\'';

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSiren(): ?int
    {
        return $this->siren;
    }

    public function setSiren(int $siren): self
    {
        $this->siren = $siren;

        return $this;
    }

    public function getNic(): ?int
    {
        return $this->nic;
    }

    public function setNic(int $nic): self
    {
        $this->nic = $nic;

        return $this;
    }

    public function getSiret(): ?string
    {
        return $this->siret;
    }

    public function setSiret(string $siret): self
    {
        $this->siret = $siret;

        return $this;
    }

    public function getDatedebut(): ?\DateTimeInterface
    {
        return $this->datedebut;
    }

    public function setDatedebut(\DateTimeInterface $datedebut): self
    {
        $this->datedebut = $datedebut;

        return $this;
    }

    public function getStatutdiffusionetablissement(): ?bool
    {
        return $this->statutdiffusionetablissement;
    }

    public function setStatutdiffusionetablissement(bool $statutdiffusionetablissement): self
    {
        $this->statutdiffusionetablissement = $statutdiffusionetablissement;

        return $this;
    }

    public function getDatecreationetablissement(): ?\DateTimeInterface
    {
        return $this->datecreationetablissement;
    }

    public function setDatecreationetablissement(\DateTimeInterface $datecreationetablissement): self
    {
        $this->datecreationetablissement = $datecreationetablissement;

        return $this;
    }

    public function getTrancheeffectifsetablissement(): ?string
    {
        return $this->trancheeffectifsetablissement;
    }

    public function setTrancheeffectifsetablissement(string $trancheeffectifsetablissement): self
    {
        $this->trancheeffectifsetablissement = $trancheeffectifsetablissement;

        return $this;
    }

    public function getAnneeeffectifsetablissement(): ?int
    {
        return $this->anneeeffectifsetablissement;
    }

    public function setAnneeeffectifsetablissement(int $anneeeffectifsetablissement): self
    {
        $this->anneeeffectifsetablissement = $anneeeffectifsetablissement;

        return $this;
    }

    public function getActiviteprincipaleregistremetiersetablissement(): ?string
    {
        return $this->activiteprincipaleregistremetiersetablissement;
    }

    public function setActiviteprincipaleregistremetiersetablissement(string $activiteprincipaleregistremetiersetablissement): self
    {
        $this->activiteprincipaleregistremetiersetablissement = $activiteprincipaleregistremetiersetablissement;

        return $this;
    }

    public function getDatederniertraitementetablissement(): ?\DateTimeInterface
    {
        return $this->datederniertraitementetablissement;
    }

    public function setDatederniertraitementetablissement(\DateTimeInterface $datederniertraitementetablissement): self
    {
        $this->datederniertraitementetablissement = $datederniertraitementetablissement;

        return $this;
    }

    public function getEtablissementsiege(): ?bool
    {
        return $this->etablissementsiege;
    }

    public function setEtablissementsiege(bool $etablissementsiege): self
    {
        $this->etablissementsiege = $etablissementsiege;

        return $this;
    }

    public function getNombreperiodesetablissement(): ?int
    {
        return $this->nombreperiodesetablissement;
    }

    public function setNombreperiodesetablissement(int $nombreperiodesetablissement): self
    {
        $this->nombreperiodesetablissement = $nombreperiodesetablissement;

        return $this;
    }

    public function getComplementadresseetablissement(): ?string
    {
        return $this->complementadresseetablissement;
    }

    public function setComplementadresseetablissement(string $complementadresseetablissement): self
    {
        $this->complementadresseetablissement = $complementadresseetablissement;

        return $this;
    }

    public function getNumerovoieetablissement(): ?string
    {
        return $this->numerovoieetablissement;
    }

    public function setNumerovoieetablissement(string $numerovoieetablissement): self
    {
        $this->numerovoieetablissement = $numerovoieetablissement;

        return $this;
    }

    public function getIndicerepetitionetablissement(): ?string
    {
        return $this->indicerepetitionetablissement;
    }

    public function setIndicerepetitionetablissement(string $indicerepetitionetablissement): self
    {
        $this->indicerepetitionetablissement = $indicerepetitionetablissement;

        return $this;
    }

    public function getTypevoieetablissement(): ?string
    {
        return $this->typevoieetablissement;
    }

    public function setTypevoieetablissement(string $typevoieetablissement): self
    {
        $this->typevoieetablissement = $typevoieetablissement;

        return $this;
    }

    public function getLibellevoieetablissement(): ?string
    {
        return $this->libellevoieetablissement;
    }

    public function setLibellevoieetablissement(string $libellevoieetablissement): self
    {
        $this->libellevoieetablissement = $libellevoieetablissement;

        return $this;
    }

    public function getCodepostaletablissement(): ?string
    {
        return $this->codepostaletablissement;
    }

    public function setCodepostaletablissement(string $codepostaletablissement): self
    {
        $this->codepostaletablissement = $codepostaletablissement;

        return $this;
    }

    public function getLibellecommuneetablissement(): ?string
    {
        return $this->libellecommuneetablissement;
    }

    public function setLibellecommuneetablissement(string $libellecommuneetablissement): self
    {
        $this->libellecommuneetablissement = $libellecommuneetablissement;

        return $this;
    }

    public function getLibellecommuneetrangeretablissement(): ?string
    {
        return $this->libellecommuneetrangeretablissement;
    }

    public function setLibellecommuneetrangeretablissement(string $libellecommuneetrangeretablissement): self
    {
        $this->libellecommuneetrangeretablissement = $libellecommuneetrangeretablissement;

        return $this;
    }

    public function getDistributionspecialeetablissement(): ?string
    {
        return $this->distributionspecialeetablissement;
    }

    public function setDistributionspecialeetablissement(string $distributionspecialeetablissement): self
    {
        $this->distributionspecialeetablissement = $distributionspecialeetablissement;

        return $this;
    }

    public function getCodecommuneetablissement(): ?string
    {
        return $this->codecommuneetablissement;
    }

    public function setCodecommuneetablissement(string $codecommuneetablissement): self
    {
        $this->codecommuneetablissement = $codecommuneetablissement;

        return $this;
    }

    public function getCodecedexetablissement(): ?string
    {
        return $this->codecedexetablissement;
    }

    public function setCodecedexetablissement(string $codecedexetablissement): self
    {
        $this->codecedexetablissement = $codecedexetablissement;

        return $this;
    }

    public function getLibellepaysetrangeretablissement(): ?string
    {
        return $this->libellepaysetrangeretablissement;
    }

    public function setLibellepaysetrangeretablissement(string $libellepaysetrangeretablissement): self
    {
        $this->libellepaysetrangeretablissement = $libellepaysetrangeretablissement;

        return $this;
    }

    public function getCodepaysetrangeretablissement(): ?string
    {
        return $this->codepaysetrangeretablissement;
    }

    public function setCodepaysetrangeretablissement(string $codepaysetrangeretablissement): self
    {
        $this->codepaysetrangeretablissement = $codepaysetrangeretablissement;

        return $this;
    }

    public function getLibellecedexetablissement(): ?string
    {
        return $this->libellecedexetablissement;
    }

    public function setLibellecedexetablissement(string $libellecedexetablissement): self
    {
        $this->libellecedexetablissement = $libellecedexetablissement;

        return $this;
    }

    public function getComplementadresse2etablissement(): ?string
    {
        return $this->complementadresse2etablissement;
    }

    public function setComplementadresse2etablissement(string $complementadresse2etablissement): self
    {
        $this->complementadresse2etablissement = $complementadresse2etablissement;

        return $this;
    }

    public function getNumerovoie2etablissement(): ?int
    {
        return $this->numerovoie2etablissement;
    }

    public function setNumerovoie2etablissement(int $numerovoie2etablissement): self
    {
        $this->numerovoie2etablissement = $numerovoie2etablissement;

        return $this;
    }

    public function getIndicerepetition2etablissement(): ?string
    {
        return $this->indicerepetition2etablissement;
    }

    public function setIndicerepetition2etablissement(string $indicerepetition2etablissement): self
    {
        $this->indicerepetition2etablissement = $indicerepetition2etablissement;

        return $this;
    }

    public function getTypevoie2etablissement(): ?string
    {
        return $this->typevoie2etablissement;
    }

    public function setTypevoie2etablissement(string $typevoie2etablissement): self
    {
        $this->typevoie2etablissement = $typevoie2etablissement;

        return $this;
    }

    public function getLibellevoie2etablissement(): ?string
    {
        return $this->libellevoie2etablissement;
    }

    public function setLibellevoie2etablissement(string $libellevoie2etablissement): self
    {
        $this->libellevoie2etablissement = $libellevoie2etablissement;

        return $this;
    }

    public function getCodepostal2etablissement(): ?string
    {
        return $this->codepostal2etablissement;
    }

    public function setCodepostal2etablissement(string $codepostal2etablissement): self
    {
        $this->codepostal2etablissement = $codepostal2etablissement;

        return $this;
    }

    public function getLibellecommune2etablissement(): ?string
    {
        return $this->libellecommune2etablissement;
    }

    public function setLibellecommune2etablissement(string $libellecommune2etablissement): self
    {
        $this->libellecommune2etablissement = $libellecommune2etablissement;

        return $this;
    }

    public function getLibellecommuneetranger2etablissement(): ?string
    {
        return $this->libellecommuneetranger2etablissement;
    }

    public function setLibellecommuneetranger2etablissement(string $libellecommuneetranger2etablissement): self
    {
        $this->libellecommuneetranger2etablissement = $libellecommuneetranger2etablissement;

        return $this;
    }

    public function getDistributionspeciale2etablissement(): ?string
    {
        return $this->distributionspeciale2etablissement;
    }

    public function setDistributionspeciale2etablissement(string $distributionspeciale2etablissement): self
    {
        $this->distributionspeciale2etablissement = $distributionspeciale2etablissement;

        return $this;
    }

    public function getCodecommune2etablissement(): ?string
    {
        return $this->codecommune2etablissement;
    }

    public function setCodecommune2etablissement(string $codecommune2etablissement): self
    {
        $this->codecommune2etablissement = $codecommune2etablissement;

        return $this;
    }

    public function getCodecedex2etablissement(): ?string
    {
        return $this->codecedex2etablissement;
    }

    public function setCodecedex2etablissement(string $codecedex2etablissement): self
    {
        $this->codecedex2etablissement = $codecedex2etablissement;

        return $this;
    }

    public function getLibellecedex2etablissement(): ?string
    {
        return $this->libellecedex2etablissement;
    }

    public function setLibellecedex2etablissement(string $libellecedex2etablissement): self
    {
        $this->libellecedex2etablissement = $libellecedex2etablissement;

        return $this;
    }

    public function getCodepaysetranger2etablissement(): ?string
    {
        return $this->codepaysetranger2etablissement;
    }

    public function setCodepaysetranger2etablissement(string $codepaysetranger2etablissement): self
    {
        $this->codepaysetranger2etablissement = $codepaysetranger2etablissement;

        return $this;
    }

    public function getLibellepaysetranger2etablissement(): ?string
    {
        return $this->libellepaysetranger2etablissement;
    }

    public function setLibellepaysetranger2etablissement(string $libellepaysetranger2etablissement): self
    {
        $this->libellepaysetranger2etablissement = $libellepaysetranger2etablissement;

        return $this;
    }

    public function getEtatadministratifetablissement(): ?string
    {
        return $this->etatadministratifetablissement;
    }

    public function setEtatadministratifetablissement(string $etatadministratifetablissement): self
    {
        $this->etatadministratifetablissement = $etatadministratifetablissement;

        return $this;
    }

    public function getEnseigne1etablissement(): ?string
    {
        return $this->enseigne1etablissement;
    }

    public function setEnseigne1etablissement(string $enseigne1etablissement): self
    {
        $this->enseigne1etablissement = $enseigne1etablissement;

        return $this;
    }

    public function getEnseigne2etablissement(): ?string
    {
        return $this->enseigne2etablissement;
    }

    public function setEnseigne2etablissement(string $enseigne2etablissement): self
    {
        $this->enseigne2etablissement = $enseigne2etablissement;

        return $this;
    }

    public function getEnseigne3etablissement(): ?string
    {
        return $this->enseigne3etablissement;
    }

    public function setEnseigne3etablissement(string $enseigne3etablissement): self
    {
        $this->enseigne3etablissement = $enseigne3etablissement;

        return $this;
    }

    public function getDenominationusuelleetablissement(): ?string
    {
        return $this->denominationusuelleetablissement;
    }

    public function setDenominationusuelleetablissement(string $denominationusuelleetablissement): self
    {
        $this->denominationusuelleetablissement = $denominationusuelleetablissement;

        return $this;
    }

    public function getActiviteprincipaleetablissement(): ?string
    {
        return $this->activiteprincipaleetablissement;
    }

    public function setActiviteprincipaleetablissement(string $activiteprincipaleetablissement): self
    {
        $this->activiteprincipaleetablissement = $activiteprincipaleetablissement;

        return $this;
    }

    public function getNomenclatureactiviteprincipaleetablissement(): ?string
    {
        return $this->nomenclatureactiviteprincipaleetablissement;
    }

    public function setNomenclatureactiviteprincipaleetablissement(string $nomenclatureactiviteprincipaleetablissement): self
    {
        $this->nomenclatureactiviteprincipaleetablissement = $nomenclatureactiviteprincipaleetablissement;

        return $this;
    }

    public function getCaractereemployeuretablissement(): ?array
    {
        return $this->caractereemployeuretablissement;
    }

    /**
     * @return $this
     */
    public function setCaractereemployeuretablissement(array $caractereemployeuretablissement): self
    {
        $this->caractereemployeuretablissement = $caractereemployeuretablissement;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}
